
# --------------------------------- Features -------------------------------- #
CONSOLE_ENABLE       = yes
TRI_LAYER_ENABLE     = yes
DEFERRED_EXEC_ENABLE = yes

# ------------------------------------ RGB ----------------------------------- #

RGB_MATRIX_CUSTOM_USER = yes

# We have a ws2812 as defined in id75/info.json
# I assume there is no reason to not include this?
# RGB_MATRIX_ENABLE = yes
# RGB_MATRIX_DRIVER = ws2812

# link time optimization:
# 	longer compile time for smaller size.
# 	Also disables some depreciated features.
# LTO_ENABLE = yes

# SRC += pdpt_util.c
# SRC += rgb/rgb.c
# SRC += rgb/anim/startup_running_dots.c

// The pin connected to the data pin of the LEDs
// #define WS2812_DI_PIN B9
// The number of LEDs connected
// #define RGB_MATRIX_LED_COUNT 89

#define DEBUG_FUNCTIONS // print function names
#define DEBUG_VERBOSE

// limits maximum brightness of LEDs to 200 out of 255. If not defined maximum brightness is set to 255
// #define MAXIMUM_BRIGHTNESS 100
// Sets the default brightness value, if none has been set
// #define RGB_MATRIX_DEFAULT_VAL 50



// set tri-layer here cause its not working in code.
#define TRI_LAYER_LOWER_LAYER 1
#define TRI_LAYER_UPPER_LAYER 2
#define TRI_LAYER_ADJUST_LAYER 5

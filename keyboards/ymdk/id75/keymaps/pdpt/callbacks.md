# Callbacks
going through the code to find _user callbacks, and write downt their location and briefs.

basically nothing is commented, so ill write down whatever I can ¯\\\_(ツ)_/¯

## led.c

* `bool led_update_user(led_t led_state)`
  * Lock LED update callback - keymap/user level

## suspend.c

* `void suspend_power_down_user(void)`
  * Run user level Power down
* `void suspend_wakeup_init_user(void)`
  * run user level code immediately after wakeup

## leader.c

* void leader_start_user(void)
* void leader_end_user(void)

## eeconfig.c

* void eeconfig_init_user(void)
  * eeconfig enable
* uint32_t eeconfig_read_user(void)
  * eeconfig read user
* void eeconfig_update_user(uint32_t val)
  * eeconfig update user
* bool eeconfig_is_user_datablock_valid(void)
  * eeconfig assert user data block version
* void eeconfig_read_user_datablock(void *data)
  * eeconfig read user data block
* void eeconfig_update_user_datablock(const void *data)
  * eeconfig update user data block
* void eeconfig_init_user_datablock(void)
  * eeconfig init user data block

## matrix_common.c

user-defined overridable functions
void matrix_init_user(void) {}  Happens midway through the firmware’s startup process. Hardware is initialized, but features may not be yet.
void matrix_scan_user(void) {}
void matrix_slave_scan_user(void) {}

## keyboard.c

keyboard_pre_init_user
Happens before most anything is started. Good for hardware setup that you want running very early.
void keyboard_pre_init_user(void) {}

keyboard_post_init_user
void keyboard_post_init_user(void) Happens at the end of the firmware’s startup process. This is where you’d want to put “customization” code, for the most part.

Override this function if you have a need to execute code for every keyboard main loop iteration.
This is specific to user/keymap-level functionality.
void housekeeping_task_user(void)

## process_music.c

bool music_mask_user(uint16_t keycode) {
    return keycode < 0xFF;
}

void music_on_user(void) {}
void midi_on_user(void) {}
void music_scale_user(void) {}

## audio.c

void audio_on_user(void) {}
void audio_off_user(void) {}

## repeat_key.c

// Default implementation of get_alt_repeat_key_keycode_user().
uint16_t get_alt_repeat_key_keycode_user(uint16_t keycode, uint8_t mods) {
    return KC_TRANSPARENT;
}

## process_repeat_key.c
// Default implementation of remember_last_key_user().
__attribute__((weak)) bool remember_last_key_user(uint16_t keycode, keyrecord_t* record, uint8_t* remembered_mods) {
    return true;
}

## process_dynamic_macro.c
related comment block:

```c
/* Handle the key events related to the dynamic macros. Should be
 * called from process_record_user() like this:
 *
 *   bool process_record_user(uint16_t keycode, keyrecord_t *record) {
 *       if (!process_record_dynamic_macro(keycode, record)) {
 *           return false;
 *       }
 *       <...THE REST OF THE FUNCTION...>
 *   }
 */
bool process_dynamic_macro(uint16_t keycode, keyrecord_t *record)
```

`/* User hooks for Dynamic Macros */ `

void dynamic_macro_record_start_user(int8_t direction) {    dynamic_macro_led_blink();}

void dynamic_macro_play_user(int8_t direction) {    dynamic_macro_led_blink();}

void dynamic_macro_record_key_user(int8_t direction, keyrecord_t *record) {    dynamic_macro_led_blink();}

void dynamic_macro_record_end_user(int8_t direction) {    dynamic_macro_led_blink();}

bool dynamic_macro_valid_key_user(uint16_t keycode, keyrecord_t *record) {    return true;}

## pointing_device.c

void pointing_device_init_user(void)
User level code pointing device initialisation

* Weak function allowing for user level mouse report modification
* Takes report_mouse_t struct allowing modification at user level then returns report_mouse_t.
* @param[in] mouse_report report_mouse_t
* @return report_mouse_t

 report_mouse_t pointing_device_task_user(report_mouse_t mouse_report)

* Weak function allowing for user level mouse report modification
* Takes 2 report_mouse_t structs allowing individual modification of sides at user level then returns pointing_device_combine_reports.
* NOTE: Only available when using SPLIT_POINTING_ENABLE and POINTING_DEVICE_COMBINED

report_mouse_t pointing_device_task_combined_user(report_mouse_t left_report, report_mouse_t right_report) {Q

## process_caps_word.c
## proccess_autocorrect.c

caps_word.c
wpm.c
encoder.c
process_steno.c
unicode.c
process_auto_shift.c
dip_switch.c
secure.c
/pointing_device_auto_mouse.c
/os_detection.c
    keycodes.h

## led_matrix.c
```c
#ifdef LED_MATRIX_CUSTOM_USER
#    include "led_matrix_user.inc"
#endif

// -----Begin led effect switch case macros-----
#define LED_MATRIX_EFFECT(name, ...)          \
    case LED_MATRIX_##name:                   \
        rendering = name(&led_effect_params); \
        break;
#include "led_matrix_effects.inc"
#undef LED_MATRIX_EFFECT

#if defined(LED_MATRIX_CUSTOM_KB) || defined(LED_MATRIX_CUSTOM_USER)
#    define LED_MATRIX_EFFECT(name, ...)          \
        case LED_MATRIX_CUSTOM_##name:            \
            rendering = name(&led_effect_params); \
            break;
#    ifdef LED_MATRIX_CUSTOM_KB
#        include "led_matrix_kb.inc"
#    endif
#    ifdef LED_MATRIX_CUSTOM_USER
#        include "led_matrix_user.inc"
#    endif
#    undef LED_MATRIX_EFFECT
#endif
            // -----End led effect switch case macros-------

__attribute__((weak)) bool led_matrix_indicators_user(void) {
    return true;
}

__attribute__((weak)) bool led_matrix_indicators_advanced_user(uint8_t led_min, uint8_t led_max) {
    return true;
}


``` 

## action_util.c

```c
/** \brief Called when the one shot modifiers have been changed.
 *
 * \param mods Contains the active modifiers active after the change.
 */
__attribute__((weak)) void oneshot_locked_mods_changed_user(uint8_t mods) {}

/** \brief Called when the one shot modifiers have been changed.
 *
 * \param mods Contains the active modifiers active after the change.
 */
__attribute__((weak)) void oneshot_mods_changed_user(uint8_t mods) {}

/** \brief Called when the one shot layers have been changed.
 *
 * \param layer Contains the layer that is toggled on, or zero when toggled off.
 */
__attribute__((weak)) void oneshot_layer_changed_user(uint8_t layer) {}
```

<!-- todo -->
## action_layer.c
```c
/** \brief Default Layer State Set At user Level
 *
 * Run user code on default layer state change
 */
__attribute__((weak)) layer_state_t default_layer_state_set_user(layer_state_t state) {
    return state;
}


/** \brief Layer state set user
 *
 * Runs user code on layer state change
 */
__attribute__((weak)) layer_state_t layer_state_set_user(layer_state_t state) {
    return state;
}
```

## quantum.c
```c
__attribute__((weak)) bool pre_process_record_user(uint16_t keycode, keyrecord_t *record) {
    return true;
}
__attribute__((weak)) bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    return true;
}
__attribute__((weak)) void post_process_record_user(uint16_t keycode, keyrecord_t *record) {}

//------------------------------------------------------------------------------
// Override these functions in your keymap file to play different tunes on
// different events such as startup and bootloader jump

__attribute__((weak)) bool shutdown_user(bool jump_to_bootloader) {
    return true;
}
```

## rgb_matrix.c

```c
#ifdef RGB_MATRIX_CUSTOM_USER
#    include "rgb_matrix_user.inc"
#endif

#if defined(RGB_MATRIX_CUSTOM_KB) || defined(RGB_MATRIX_CUSTOM_USER)
#    define RGB_MATRIX_EFFECT(name, ...)          \
        case RGB_MATRIX_CUSTOM_##name:            \
            rendering = name(&rgb_effect_params); \
            break;
#    ifdef RGB_MATRIX_CUSTOM_KB
#        include "rgb_matrix_kb.inc"
#    endif
#    ifdef RGB_MATRIX_CUSTOM_USER
#        include "rgb_matrix_user.inc"
#    endif
#    undef RGB_MATRIX_EFFECT
#endif


__attribute__((weak)) bool rgb_matrix_indicators_user(void) {
    return true;
}


__attribute__((weak)) bool rgb_matrix_indicators_advanced_user(uint8_t led_min, uint8_t led_max) {
    return true;
}
```

# id75 pdpt's personal Layout

![id75/pdpt Layout Image](https://i.imgur.com/7Capi8W.png)

This is my personal layout. It is originally based off of my planck/ez layout.

There are also personal notes contained here, mostly references to documentation I think is important. These are contained at the bottom, under the *Personal Notes* section.

This is the default layout that comes flashed on every Clueboard. For the most
part it's a straightforward and easy to follow layout. The only unusual key is
the key in the upper left, which sends Escape normally, but Grave when any of
the Ctrl, Alt, or GUI modifiers are held down.

## Personal Notes

### important links

* docs: <https://docs.qmk.fm/>
  * keyboard guidelines: <https://docs.qmk.fm/#/hardware_keyboard_guidelines>
  * rgb_matrix: <https://docs.qmk.fm/#/feature_rgb_matrix>
    <!--? ws2812 -->
* drashna userspace: <https://github.com/drashna/qmk_userspace/>
* userspace documentation: <https://docs.qmk.fm/#/feature_userspace>

### general structure

As I understand it, a good structure would be:

* custom functions, features, etc.. go in userspace
* static settings for those functions go in the keymap/config.h
  * if there are any settings I want on all the time, they'd go in user/config.h
* custom feature names are defined in user/rules.mk
* user/name.c is probably going to be mostly imports?
  * can define function overwrites here, but makes sense to me if I'd move them to their own file if possible.
* user/other.c for any custom function such as an rgb effect.

### userspace

* /users/\<name\>/ (added to the path automatically)
  * readme.md (optional, recommended)
  * rules.mk (included automatically)
    * how you add additional source files
    * where you add custom features
  * config.h (included automatically)
    * hardware options
    * features enable/disable
    * behavior config
      * (i.e. tapping term)
    * rgb light config
      * (i.e. brightness step)
  * \<name\>.c/.h (optional)
    * where you can overwrite functions see: "customized functions"
  * cool_rgb_stuff.c/.h (optional)

You should use the config.h for configuration options, and the \<name\>.h file for user or keymap specific settings (such as the enum for layer or keycodes)

### keyboard guidelines

#### keyboard folder structure

this is for keyboards, not keymaps. But I am hoping it will help me determine what files qmk recognises & what their purpose is.

* readme.md
  * Keymap readme.md Template: <https://docs.qmk.fm/#/documentation_templates?id=keyboard-readmemd-template>
  * contains a template, and links to a website to make images of your keymap.
    * you should include an image at the top of your readme file.
* info.json
  * the hardware and feature set for your keyboard.
  * doesn't need to be edited, but can be a useful reference.
* config.h
* rules.mk
* <keyboard_name>.c
* <keyboard_name>.h

##### info.json

* "manufacturer": "YMDK",
* "keyboard_name": "Idobao x YMDK ID75",
* "maintainer": "qmk",
* "bootloader": "uf2boot",
* "features":
  * "bootmagic": true,
  * "extrakey": true,
  * "mousekey": true,
  * "nkro": true,
  * "rgb_matrix": true
* "processor": "STM32F103",
* rgb_matrix
  * driver: "ws2812",

The keyboard does have [RGB Underglow](https://docs.qmk.fm/#/feature_rgblight), but I believe it is simply part of the rgb_matrix. So it is not listed in `info.json`

* config.h
  * sets things like the matrix size, product name, USB VID/PID, description and other settings. In general, use this file to set essential information and defaults for your keyboard that will always work.

### RGB

LEDS are defined in info.json "rgb_matrix" "layout".

``` c
typedef struct PACKED {
    uint8_t     matrix_co[MATRIX_ROWS][MATRIX_COLS];
    led_point_t point[RGB_MATRIX_LED_COUNT];
    uint8_t     flags[RGB_MATRIX_LED_COUNT];
} led_config_t;
```

`g_led_config` seems to be the variable to use in .c files.

* matrix_co\[row]\[col] - I believe this is the index of the LED
  * i.e. `g_led_config.matrix_co\[5]\[2] would return the index of the led at (2, 5).
* point \[index] - gets the physical position of a specific LED
* flags \[index] - gets the flags for a specific LED
  * `2` is for underglow
  * `4` is for everything else

* `rgb_matrix_config`
  * enable
  * mode
  * hsv
  * speed
  * flags

see: <https://github.com/drashna/qmk_userspace/blob/master/keyboards/massdrop/alt/keymaps/drashna/keymap.c>
for code that changes RGB_TOG to cycle through some specific options.

<!-- todo:  -->
how should I set per-keymap led flags?

* in some post_init function
  * g_led_config.flags[i] = LED_FLAG

/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//* important page: https://docs.qmk.fm/#/custom_quantum_functions
//* planck ez code: https://github.com/qmk/qmk_firmware/tree/master/keyboards/planck/ez

#include <stdint.h>
#include "rgb_matrix.h"
#include QMK_KEYBOARD_H

enum layer_names {
    _BL,
    _FL,
    _CL,
};

// clang-format off

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [0] = LAYOUT_ortho_5x15(
        MO(1), KC_1   , KC_2   , KC_3  , KC_4  , KC_5  , KC_6  , KC_7  , KC_8   , KC_9   , KC_0   , KC_MINS, KC_EQL , KC_BSLS, KC_GRV ,
        MO(1), KC_Q   , KC_W   , KC_E  , KC_R  , KC_T  , KC_Y  , KC_U  , KC_I   , KC_O   , KC_P   , KC_LBRC, KC_RBRC, KC_BSPC, KC_DEL ,
        MO(1), KC_A   , KC_S   , KC_D  , KC_F  , KC_G  , KC_H  , KC_J  , KC_K   , KC_L   , KC_SCLN, KC_QUOT, KC_ENT , KC_ENT , KC_PGUP,
        MO(1), KC_Z   , KC_X   , KC_C  , KC_V  , KC_B  , KC_N  , KC_M  , KC_COMM, KC_DOT , KC_SLSH, KC_RSFT, MO(1), KC_UP  , KC_PGDN,
        MO(1), KC_LGUI, KC_LALT, KC_SPC, KC_SPC, KC_SPC, KC_SPC, KC_SPC, KC_SPC , KC_RALT, KC_RCTL, KC_RCTL, KC_LEFT, KC_DOWN, KC_RGHT
    ),
    [1] = LAYOUT_ortho_5x15(
        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
        RGB_MOD, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, QK_BOOT
    )
};

// clang-format on
#if defined(ENCODER_ENABLE) && defined(ENCODER_MAP_ENABLE)
const uint16_t PROGMEM encoder_map[][NUM_ENCODERS][NUM_DIRECTIONS] = {

};
#endif // defined(ENCODER_ENABLE) && defined(ENCODER_MAP_ENABLE)

/**
 */
// This is where you’d want to put “customization” code, for the most part.
/**
 * Happens at the end of the firmware’s startup process.
 * This is where you’d want to put “customization” code, for the most part.
 * For instance, this is where you want to set up things for RGB Underglow.
 *
 *
 * */
void keyboard_post_init_user(void) {
    // Customise these values to desired behaviour
    debug_enable   = true;
    debug_matrix   = true;
    debug_keyboard = true;
    // debug_mouse=true;

    // load custom RGB Effect
    // // rgb_matrix_mode_noeeprom(RGB_MATRIX_CUSTOM_default_rgb);
    // // rgb_matrix_mode_noeeprom(RGB_MATRIX_CUSTOM_my_cool_effect2);
    rgb_matrix_mode_noeeprom(RGB_MATRIX_SOLID_COLOR);
    // // rgb_matrix_mode_noeeprom(RGB_MATRIX_BAND_SAT);
    // for (uint8_t i = 0; i < 20; i++) {
    //     // rgb_matrix_decrease_val_noeeprom();
    // }

#ifdef CONSOLE_ENABLE
    uint16_t mode = rgb_matrix_get_mode();

    uprintf("initial mode: %u\n", mode);
#endif
}

/**
 * When you want to override the behavior of an existing key, or define the behavior for a new key,
 * you should use the process_record_kb() and process_record_user() functions.
 *
 * * If return true, QMK will process the keycodes as usual.
 * * If return false, QMK will skip the normal key handling.
 *
 * Whenever possible you should customize your keyboard by using process_record_*()
 *  and hooking into events that way,
 * to ensure that your code does not have a negative performance impact on your keyboard.
 *
 * The <keycode> argument is whatever is defined in your keymap, eg MO(1), KC_L, etc.
 * You should use a switch...case block to handle these events.
 *
 * The <record> argument contains information about the actual press:
 * * bool    pressed
 * * uin16_t time
 * * uin16_t event.key.row
 * * uin16_t event.key.col
 * */
bool process_record_user(uint16_t keycode, keyrecord_t* record) {
// If console is enabled, it will print the matrix position and status of each key pressed
#ifdef CONSOLE_ENABLE
    uint16_t mode = rgb_matrix_get_mode();
    uprintf("KL: kc: 0x%04X, col: %2u, row: %2u, pressed: %u, time: %5u, int: %u, count: %u\n", keycode, record->event.key.col, record->event.key.row, record->event.pressed, record->event.time, record->tap.interrupted, record->tap.count);
    uprintf("mode: %u\n", mode);
#endif

    switch (keycode) {
        case KC_SPC:
            rgb_matrix_mode_noeeprom(RGB_MATRIX_SOLID_COLOR);
            return FALSE;
    }
    return TRUE;
}

bool rgb_matrix_indicators_user(void) {
    // uint8_t keyCount = 75;
    //
    // for (uint8_t i = 0; i < keyCount; i++)
    // {
    // 	bool fizz = ((i%3) == 0) ? 1 : 0;
    // 	bool buzz = ((i%5) == 0) ? 1 : 0;
    // 	if (fizz && buzz){
    // 		rgb_matrix_set_color(i, RGB_RED);
    // 	} else if (fizz){
    // 		rgb_matrix_set_color(i, RGB_BLUE);
    // 	} else if (buzz){
    // 		rgb_matrix_set_color(i, RGB_YELLOW);
    // 	} else {
    // 		rgb_matrix_set_color(i, RGB_GREEN);
    // 	}
    // }

    return true;
}

layer_state_t layer_state_set_user(layer_state_t state) {
    //     switch (get_highest_layer(state)) {
    //     case _RAISE:
    //         rgblight_setrgb (0x00,  0x00, 0xFF);
    //         break;
    //     case _LOWER:
    //         rgblight_setrgb (0xFF,  0x00, 0x00);
    //         break;
    //     case _PLOVER:
    //         rgblight_setrgb (0x00,  0xFF, 0x00);
    //         break;
    //     case _ADJUST:
    //         rgblight_setrgb (0x7A,  0x00, 0xFF);
    //         break;
    //     default: //  for any other layers, or the default layer
    //         rgblight_setrgb (0x00,  0xFF, 0xFF);
    //         break;
    //     }
    return state;
}

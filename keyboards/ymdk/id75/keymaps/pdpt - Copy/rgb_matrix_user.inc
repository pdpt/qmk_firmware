RGB_MATRIX_EFFECT(my_cool_effect)
RGB_MATRIX_EFFECT(my_cool_effect2)

#ifdef RGB_MATRIX_CUSTOM_EFFECT_IMPLS

static bool my_cool_effect(effect_params_t* params) {
    // color array
    uint8_t colorArray[5][3] = {{0x00, 0xFF, 0x80}, {RGB_MAGENTA}, {RGB_PINK}, {RGB_GREEN}, {RGB_PURPLE}};
    // color index
    uint8_t colorIndex = 0;

    RGB_MATRIX_USE_LIMITS(led_min, led_max);

    for (uint8_t i = led_min; i < led_max; i++) {
        uint8_t r = colorArray[colorIndex][0];
        uint8_t g = colorArray[colorIndex][1];
        uint8_t b = colorArray[colorIndex][2];
        rgb_matrix_set_color(i, r, g, b);
        if (i % 2 == 0) {
            colorIndex++;
        }
    }

    return rgb_matrix_check_finished_leds(led_max);
}

static bool my_cool_effect2(effect_params_t* params) {
    RGB_MATRIX_USE_LIMITS(led_min, led_max);

    for (uint8_t i = led_min; i < led_max; i++) {
        rgb_matrix_set_color(i, RGB_PURPLE);
    }

    return rgb_matrix_check_finished_leds(led_max);
}

#endif

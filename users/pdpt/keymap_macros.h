#pragma once
#include "pdpt.h"
#include QMK_KEYBOARD_H

/**
 *
 * @brief blocks to group common keycodes together, to make keymaps easier to make and more readable.
 *
 * they are sized to match the 7 width keycodes.
 */

// clang-format off
/* ------------------------------- left blocks ------------------------------ */
#define _________________12345__________________     KC_1,    KC_2,    KC_3,    KC_4,    KC_5
#define _________________qwert__________________     KC_Q,    KC_W,    KC_E,    KC_R,    KC_T
#define _________________asdfg__________________     KC_A,    KC_S,    KC_D,    KC_F,    KC_G
#define _________________zxcvb__________________     KC_Z,    KC_X,    KC_C,    KC_V,    KC_B

/* ------------------------------ right blocks ------------------------------ */
#define _________________67890__________________     KC_6,    KC_7,    KC_8,    KC_9,    KC_0
#define _________________yuiop__________________     KC_Y,    KC_U,    KC_I,    KC_O,    KC_P
#define _____________hjkl______________              KC_H,    KC_J,    KC_K,    KC_L
#define _____nm______                                KC_N,    KC_M
// clang-format on

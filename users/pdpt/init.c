/*
 * @file        	\qmk_firmware\users\pdpt\init.c
 * @fileoverview	Contains functions related to keyboard_init
 * @see         	<<filelink>>
 *
 * @author	pdpt
 * @date  	2024-03-11
 */

/**
 * @brief This is where you'd want to put "customization" code.
 *
 * @details
 *      Happens at the end of the firmware’s startup process.
 *      This is where you would want to set up things like RGB underglow.
 *
 */
void keyboard_post_init_user(void) {
    // Customise these values to desired behaviour
    debug_enable   = TRUE;
    debug_matrix   = FALSE;  // prints the keyboard as a "matrix" each keypress.
    debug_keyboard = TRUE;
    debug_mouse    = FALSE;

    set_tri_layer_layers(_LOWER, _RAISE, _ADJUST);

    // rgb_matrix_mode_noeeprom(RGB_MATRIX_SOLID_COLOR);
    // load custom RGB Effect
    // keyboard_post_init_rgb_matrix();


#ifdef CONSOLE_ENABLE
#    ifdef DEBUG_FUNCTIONS
    // print_function("keyboard_post_init_user");
#    endif
    uint16_t mode = rgb_matrix_get_mode();

    uprintf("initial mode: %u\n", mode);
#endif
}

/**
 * @brief run user level code immediately after wakeup
 *
 */
void suspend_wakeup_init_user(void) {

}

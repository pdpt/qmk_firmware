/*
 * @header Copyright 2024 Taylor Premo (pdpt@gitlab.com) <pdpt@pm.me>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * @file        	callbacks.c
 */

#include "callbacks.h" // IWYU pragma: keep.

#include "pdpt.h"
#include "rgb/rgb_matrix_stuff.h"

#include "quantum/color.h"
#include "quantum/rgb_matrix/rgb_matrix.h"

/**
 * @brief This runs code every time that the layers get changed. This can be
 * useful for layer indication, or custom layer handling.
 *
 * @note the rgb on layer stuff seems to not work well with rgb_matrix
 *
 * @param state
 * @return layer_state_t
 */
layer_state_t layer_state_set_user(layer_state_t state) {
    state = update_tri_layer_state(state, _LOWER, _RAISE, _ADJUST);
    return state;
}

/**
 * @brief This is where you'd want to put "customization" code.
 *
 * @details
 *      Happens at the end of the firmware’s startup process.
 *      This is where you would want to set up things like RGB underglow.
 *
 */
void keyboard_post_init_user(void) {
    // Customise these values to desired behaviour
    debug_enable   = TRUE;
    debug_matrix   = FALSE; // prints the keyboard as a "matrix" each keypress.
    debug_keyboard = TRUE;
    debug_mouse    = FALSE;

    userspace_config.rgb_layer_change = true;
    userspace_config.counter          = 0;

    set_tri_layer_layers(_LOWER, _RAISE, _ADJUST);

    // * must set initial HSV or brightness will be fucked up.
    rgb_matrix_sethsv_noeeprom(HSV_WHITE);
}

/**
 * @brief run user level code immediately after wakeup
 *
 */
void suspend_wakeup_init_user(void) {}

void housekeeping_task_user(void) {}

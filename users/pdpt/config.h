// term for TT function

#define RGB_MATRIX_SLEEP // turn off effects when suspended
// number of milliseconds to wait until led automatically turns off
#define RGB_MATRIX_TIMEOUT 0

// #define TEST_PROGRAM
// use layer change or indicator function
#define RGB_USE_IND_FUNC

// #define TAPPING_TERM 100
#define TAPPING_TOGGLE 2

// Configure the global tapping term (default: 200ms)
#define TAPPING_TERM 200

// Enable rapid switch from tap to hold, disables double tap hold auto-repeat.
#define QUICK_TAP_TERM 0

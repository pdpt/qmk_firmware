# todo: add documentation here

#
# Simple assignment      `:=`	A simple assignment expression is evaluated only once, at the very first occurrence.
# Recursive assignment    `=`	A Recursive assignment expression is evaluated everytime the variable is encountered in the code.
# Conditional assignment `?=`	Conditional assignment assigns a value to a variable only if it does not have a value
# Appending              `+=`	Assume that `CC = gcc` then the appending operator is used like `CC += -w` then `CC` now has the value `gcc -W`
SRC +=  $(USER_PATH)/pdpt.c \
        $(USER_PATH)/callbacks.c \
        $(USER_PATH)/process_keys/process_records.c \
        $(USER_PATH)/rgb/rgb_matrix_stuff.c \
        $(USER_PATH)/rgb/anim/led_counter.c \
        # $(USER_PATH)/eeconfig_users.c

DEFERRED_EXEC_ENABLE ?= yes

# -include						won't generate an error if the include file doesn't exist.

# include other .mk files at the end
# include $(USER_PATH)/oled/rules.mk
# include $(USER_PATH)/pointing/rules.mk
# include $(USER_PATH)/split/rules.mk
# include $(USER_PATH)/painter/rules.mk
# include $(USER_PATH)/rgb/rules.mk
# include $(USER_PATH)/keyrecords/rules.mk
# include $(USER_PATH)/features/rules.mk
# Ignore if not found
-include $(KEYMAP_PATH)/post_rules.mk

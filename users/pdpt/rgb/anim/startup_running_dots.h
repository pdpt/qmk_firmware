/*
 * 2024-03-07 PDPT <pdpt@pm.me>
 */
#ifndef RUNNING_DOTS_H
#define RUNNING_DOTS_H
bool running_dots(uint8_t h, uint8_t s, uint8_t v);
#endif

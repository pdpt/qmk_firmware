#include "led_counter.h"
#include "rgb_matrix.h"
#include "timer.h"
#include <stdbool.h>
#include <stdint.h>
#include QMK_KEYBOARD_H

static const  uint8_t led_count       = 75 + 14;
static const uint16_t update_interval = 500;
static const     bool do_set_off      = true;

static uint32_t rgb_timer = 0;
static  uint8_t led_index = 0;

/**
 *  LED COUNTER
 *
 * sets each LED to a color, one at a time.
 * mainly as a test to ensure all LEDs are working as intended.
 *
 * @return   true
 * @return   false
 */
bool LED_COUNTER(void) {
    if(timer_elapsed32(rgb_timer) > update_interval) {
        rgb_timer = timer_read32();
        led_index   = (led_index + 1)  % led_count;
    }

    if(do_set_off){
        rgb_matrix_set_color_all(RGB_OFF);
    }
    rgb_matrix_set_color(led_index, RGB_RED);

    return false;
}

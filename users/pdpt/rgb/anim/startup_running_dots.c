/*
 * 2024-03-07 PDPT <pdpt@pm.me>
 */
#include QMK_KEYBOARD_H

#define DECAY 1

static HSV hsv;
static uint8_t index_array[RGB_MATRIX_LED_COUNT];



/**
 * @brief starting at index, walks backwards and decays
 *
 */
void update_all(uint8_t index){

    // HSV cur_hsv = hsv;
    // RGB rgb = hsv_to_rgb(cur_hsv);

    // // rgb_matrix_set_color(index, rgb.r, rgb.g, rgb.b);
    // index--;

    // while(index > 0){
    //     // stop if we've reached black LEDs
    //     if(cur_hsv.v <= DECAY){
    //         break;
    //     }
    //     // darken by DECAY
    //     cur_hsv.v -= DECAY;
    //     //set the new color
    //     rgb =  hsv_to_rgb(cur_hsv);
    //     // rgb_matrix_set_color(index, rgb.r, rgb.g, rgb.b);
    //     // walk backwards
    //     index--;
    // }

}

/**
 * @brief assuming the array is correctly set, updates the position of the runner.
 *
 */
void run(void){
////    for (uint8_t index = 0; index < RGB_MATRIX_LED_COUNT; index++) {
////        update_all(index);
////    }
////
}


/**
 * @brief startup animation where a dot runs back and forth
 *
 * @return true
 * @return false
 */
uint32_t running_dots(uint32_t triger_time, uint8_t h, uint8_t s, uint8_t v){
////    // is_rgb_matrix_startup = true;
////    HSV temp = {h, s, v};
////    hsv = temp;
////    //g_led_config
////
////    // fill the array with the LEDs in order
////    // start at top left, running across
////    uint8_t i = 0;
////    for (uint8_t pos_y = 0; pos_y < MATRIX_ROWS; pos_y++) {
////        uint8_t pos_x;
////        for (pos_x = 0; pos_x < MATRIX_COLS; pos_x++) {
////            // store the indexes in traversal order.
////            // index_array[i] = g_led_config.matrix_co[pos_x][pos_y];
////            index_array[i] = (pos_y * MATRIX_COLS) + pos_x;
////            i++;
////        }
////        // once you get to the end, turn around and move down one
////        pos_y++;
////        for (; pos_x > 0; pos_x--) {
////            // store the indexes in traversal order.
////            // index_array[i] = g_led_config.matrix_co[pos_x][pos_y];
////            index_array[i] = (pos_y * MATRIX_COLS) + pos_x;
////            i++;
////        }
////    }
////
////    run();
////
////    // is_rgb_matrix_startup = false;
////
////    // return is_rgb_matrix_startup ? 10 : 0;;
////    return false;
////
////    // brightness = RGB_MATRIX_DEFAULT_VAL
////
////    // when you leave an LED, it begins to decay
////    // stop updating an LED once it has no value
}

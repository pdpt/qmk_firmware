/*
 * @header Copyright 2024 Taylor Premo (pdpt@gitlab.com) <pdpt@pm.me>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * @file        	rgb_matrix_stuff.h
 */

#include "action_layer.h"
#include "color.h"

#ifndef RGB_MATRIX_STUFF_H_
#    define RGB_MATRIX_STUFF_H_
bool rgb_matrix_indicators_keymap(void);

// bool process_record_user_rgb_matrix(uint16_t keycode, keyrecord_t *record);
// void keyboard_post_init_rgb_matrix(void);
// void housekeeping_task_rgb_matrix(void);

// void rgb_matrix_set_color_all(uint8_t red, uint8_t green, uint8_t blue);
// void rgb_matrix_layer_helper(uint8_t hue, uint8_t sat, uint8_t val, uint8_t
// mode, uint8_t speed, uint8_t led_type,
//                              uint8_t led_min, uint8_t led_max);

// bool rgb_matrix_indicators_advanced_keymap(uint8_t led_min, uint8_t led_max);
// bool rgb_matrix_indicators_keymap(void);
// void rgb_matrix_shutdown(bool jump_to_bootloader);

// const char *rgb_matrix_name(uint8_t effect);

// returns a HSV struct based on the current layer.
HSV get_layer_hsv(layer_state_t state);

void rgb_matrix_set_led_flags(void);

#endif /* !RGB_MATRIX_STUFF_H_ */

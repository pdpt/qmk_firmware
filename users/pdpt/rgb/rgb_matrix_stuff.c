/*
 * @header Copyright 2024 Taylor Premo (pdpt@gitlab.com) <pdpt@pm.me>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * @file        	rgb_matrix_stuff.c
 */

#include "rgb_matrix_stuff.h"

// #include "action_layer.h" // IWYU pragma: keep
#include "color.h"
// #include "info_config.h" // IWYU pragma: keep
// #include "matrix.h" // IWYU pragma: keep
#include "pdpt.h"
// #include "callbacks.h" // IWYU pragma: keep
#include "anim/led_counter.h"

// #include "print.h" // IWYU pragma: keep
// #include "quantum/rgb_matrix/rgb_matrix_types.h" // IWYU pragma: keep
#include "quantum/rgb_matrix/rgb_matrix.h"

// #include "lib/lib8tion/lib8tion.h" // IWYU pragma: keep
// #include <stdint.h>

//// static uint32_t rgb_timer = 0;

bool rgb_matrix_indicators_user(void) {
    return LED_COUNTER();

////     uint32_t led_count = 75 + 14;
////     uint32_t update_interval = 500;
////    if (timer_elapsed32(rgb_timer) > update_interval) {
////        rgb_timer                 = timer_read32();
////        userspace_config.counter += 1;
////        userspace_config.counter  = userspace_config.counter % led_count;
////    }
////
////    rgb_matrix_set_color_all(RGB_OFF);
////    rgb_matrix_set_color(userspace_config.counter, RGB_RED);
////    return false;
}

#ifdef UNDEF
RGB rgb_matrix_hsv_to_rgb(void) {
////    HSV hsv = rgb_matrix_get_hsv();
////    RGB rgb = hsv_to_rgb(hsv);
////    return rgb;
}

RGB get_layer_rgb(layer_state_t state) {
////     RGB rgb;
////
////     switch (biton32(state)) { // biton is faster than get_highest_layer for some reason.
////         case _ADJUST:
////             uprintf("Layer: Adjust\n");
////             uprintf("\t %d, %d, %d\n", HSV_WHITE);
////             // rgb_matrix_set_by_flag(HSV_WHITE, mode, flag, led_min, led_max);
////             // rgb_matrix_sethsv_noeeprom(HSV_WHITE);
////             rgb = (RGB){RGB_WHITE};
////             break;
////         case _LOWER:
////             uprintf("Layer: _LOWER\n");
////             uprintf("\t %d, %d, %d\n", HSV_BLUE);
////             // uprintf("Lower\n");
////             // rgb_matrix_set_by_flag(HSV_BLUE, mode, flag, led_min, led_max);
////             // rgb_matrix_sethsv_noeeprom(HSV_BLUE);
////             rgb = (RGB){RGB_BLUE};
////             break;
////         case _RAISE:
////             uprintf("Layer: _RAISE\n");
////             uprintf("\t %d, %d, %d\n", HSV_CYAN);
////             // uprintf("Raise\n");
////             // rgb_matrix_set_by_flag(HSV_CYAN, mode, flag, led_min, led_max);
////             // rgb_matrix_sethsv_noeeprom(HSV_CYAN);
////             rgb = (RGB){RGB_CYAN};
////             break;
////         case _NUMPAD:
////             uprintf("Layer: _NUMPAD\n");
////             uprintf("\t %d, %d, %d\n", HSV_YELLOW);
////             // uprintf("Numpad\n");
////             // rgb_matrix_set_by_flag(HSV_YELLOW, mode, flag, led_min, led_max);
////             // rgb_matrix_sethsv_noeeprom(HSV_YELLOW);
////             rgb = (RGB){RGB_YELLOW};
////             break;
////         case _NAV:
////             uprintf("Layer: _NAV\n");
////             uprintf("\t %d, %d, %d\n", HSV_PURPLE);
////             // uprintf("Navigation\n");
////             // rgb_matrix_set_by_flag(HSV_PURPLE, mode, flag, led_min, led_max);
////             // rgb_matrix_sethsv_noeeprom(HSV_PURPLE);
////             rgb = (RGB){RGB_PURPLE};
////             break;
////         case _BASE:
////             uprintf("Layer: _BASE\n");
////             uprintf("\t %d, %d, %d\n", HSV_GREEN);
////         default:
////             // uprintf("Base\n");
////             // rgb_matrix_set_by_flag(HSV_GREEN, mode, flag, led_min, led_max);
////             // rgb_matrix_sethsv_noeeprom(HSV_GREEN);
////             rgb = (RGB){RGB_GREEN};
////             break;
////     }
////
////     return rgb;
//// }
////
//// HSV get_layer_hsv(layer_state_t state) {
////     uprintf("layer state: %d", state);
////     HSV hsv;
////     // uint8_t mode = RGB_MATRIX_SOLID_COLOR;
////     // // uint8_t flag = LED_FLAG_UNDERGLOW;
////     // uint8_t flag = LED_FLAG_KEYLIGHT;
////     // switch (get_highest_layer(layer_state)) {
////     switch (biton32(state)) { // biton is faster than get_highest_layer for some reason.
////         case _ADJUST:
////             uprintf("Layer: Adjust\n");
////             uprintf("\t %d, %d, %d\n", HSV_WHITE);
////             // rgb_matrix_set_by_flag(HSV_WHITE, mode, flag, led_min, led_max);
////             // rgb_matrix_sethsv_noeeprom(HSV_WHITE);
////             hsv = (struct HSV){HSV_WHITE};
////             break;
////         case _LOWER:
////             uprintf("Layer: _LOWER\n");
////             uprintf("\t %d, %d, %d\n", HSV_BLUE);
////             // uprintf("Lower\n");
////             // rgb_matrix_set_by_flag(HSV_BLUE, mode, flag, led_min, led_max);
////             // rgb_matrix_sethsv_noeeprom(HSV_BLUE);
////             hsv = (struct HSV){HSV_BLUE};
////             break;
////         case _RAISE:
////             uprintf("Layer: _RAISE\n");
////             uprintf("\t %d, %d, %d\n", HSV_CYAN);
////             // uprintf("Raise\n");
////             // rgb_matrix_set_by_flag(HSV_CYAN, mode, flag, led_min, led_max);
////             // rgb_matrix_sethsv_noeeprom(HSV_CYAN);
////             hsv = (struct HSV){HSV_CYAN};
////             break;
////         case _NUMPAD:
////             uprintf("Layer: _NUMPAD\n");
////             uprintf("\t %d, %d, %d\n", HSV_YELLOW);
////             // uprintf("Numpad\n");
////             // rgb_matrix_set_by_flag(HSV_YELLOW, mode, flag, led_min, led_max);
////             // rgb_matrix_sethsv_noeeprom(HSV_YELLOW);
////             hsv = (struct HSV){HSV_YELLOW};
////             break;
////         case _NAV:
////             uprintf("Layer: _NAV\n");
////             uprintf("\t %d, %d, %d\n", HSV_PURPLE);
////             // uprintf("Navigation\n");
////             // rgb_matrix_set_by_flag(HSV_PURPLE, mode, flag, led_min, led_max);
////             // rgb_matrix_sethsv_noeeprom(HSV_PURPLE);
////             hsv = (struct HSV){HSV_PURPLE};
////             break;
////         case _BASE:
////             uprintf("Layer: _BASE\n");
////             uprintf("\t %d, %d, %d\n", HSV_GREEN);
////         default:
////             // uprintf("Base\n");
////             // rgb_matrix_set_by_flag(HSV_GREEN, mode, flag, led_min, led_max);
////             // rgb_matrix_sethsv_noeeprom(HSV_GREEN);
////             hsv = (struct HSV){HSV_GREEN};
////             break;
////     }
////
////     return hsv;
//// }
////
//// // ? I think the issue is the underglow LEDs are below led_min
//// void rgb_matrix_set_by_flag(uint8_t hue, uint8_t sat, uint8_t val, uint8_t mode, uint8_t led_flag, uint8_t led_min, uint8_t led_max) {
////     HSV hsv = {hue, sat, val};
////     if (hsv.v > rgb_matrix_get_val()) {
////         hsv.v = rgb_matrix_get_val();
////     }
////
////     switch (mode) {
////         case RGB_MATRIX_SOLID_COLOR: {
////             RGB rgb = hsv_to_rgb(hsv);
////             for (uint8_t i = 0; i < RGB_MATRIX_LED_COUNT; i++) {
////                 // if(do_print){
////                 // uprintf("%d: %d = %d: %d\n", i, g_led_config.flags[i],
////                 // led_flag), RGB_MATRIX_LED_COUNT;
////                 // }
////                 if (HAS_FLAGS(g_led_config.flags[i], led_flag)) {
////                     uprintf("flag match");
////                     // this function requires led_min & led_max for some reason
////                     // :( RGB_MATRIX_INDICATOR_SET_COLOR(i, rgb.r, rgb.g,
////                     // rgb.b);
////                     rgb_matrix_set_color(i, rgb.r, rgb.g, rgb.b);
////                 }
////             }
////             break;
////         }
////     }
////     // do_print = false;
//// }
//// __attribute__((weak)) bool rgb_matrix_indicators_keymap(void) {
////     return true;
//// }
////
//// bool rgb_matrix_indicators_user(void) {
////     return rgb_matrix_indicators_keymap();
//// }
////
//// void set_led_helper(uint8_t index, uint8_t hue, uint8_t sat, uint8_t val, uint8_t led_min, uint8_t led_max) {
////     HSV hsv = {hue, sat, val};
////     if (hsv.v > rgb_matrix_get_val()) {
////         hsv.v = rgb_matrix_get_val();
////     }
////
////     // RGB rgb = rgb_matrix_hsv_to_rgb();
////     RGB rgb = hsv_to_rgb(hsv);
////     RGB_MATRIX_INDICATOR_SET_COLOR(index, rgb.r, rgb.g, rgb.b);
//// }
////
//// void rgb_matrix_layer_helper(uint8_t hue, uint8_t sat, uint8_t val, uint8_t mode, uint8_t speed, uint8_t led_type, uint8_t led_min, uint8_t led_max) {
////     // RGB_MATRIX_INDICATOR_SET_COLOR(31, 0x00, 0xFF, 0xFF); // W
////     uint8_t led_count = RGB_MATRIX_LED_COUNT;
////
////     HSV hsv = {hue, sat, val};
////     uprintf("\n\tled_matrix_helper hsv: {%d, %d, %d}\n", hsv.h, hsv.s, hsv.v);
////     if (hsv.v > rgb_matrix_get_val()) {
////         hsv.v = rgb_matrix_get_val();
////     }
////
////     // RGB rgb = rgb_matrix_hsv_to_rgb();
////     RGB rgb = hsv_to_rgb(hsv);
////
////     uprintf("\tled_matrix_helper rgb: {%d, %d, %d}\n", rgb.r, rgb.g, rgb.b);
////     for (uint8_t i = 0; i < led_count; i++) {
////         // if (i < 5) {
////         //     set_led_helper(i, HSV_BLUE, led_min, led_max);
////         //     continue;
////         // }
////         if (i == 12) {
////             set_led_helper(i, HSV_RED, led_min, led_max);
////             continue;
////         }
////         // if (i < 13) {
////         //     set_led_helper(i, HSV_GREEN, led_min, led_max);
////         //     continue;
////         // }
////         // if (i < 14) {
////         //     set_led_helper(i, HSV_YELLOW, led_min, led_max);
////         //     continue;
////         // }
////         // if (i < 15) {
////         //     set_led_helper(i, HSV_PINK, led_min, led_max);
////         //     continue;
////         // }
////         // if (i < 16) {
////         //     set_led_helper(i, HSV_PURPLE, led_min, led_max);
////         //     continue;
////         // }
////
////         set_led_helper(i, HSV_WHITE, led_min, led_max);
////
////         /*
////         if (i > 7) {
////             HSV hsv_blue = {HSV_BLUE};
////             if (hsv_blue.v > rgb_matrix_get_val()) {
////                 hsv_blue.v = rgb_matrix_get_val();
////             }
////             RGB rgb_blue = hsv_to_rgb(hsv_blue);
////             RGB_MATRIX_INDICATOR_SET_COLOR(i, rgb_blue.r, rgb_blue.g, rgb_blue.b);
////         } else
////             // if (HAS_ANY_FLAGS(g_led_config.flags[i], led_type)) {
////             if (i < 14) { //     0..13 are underglow
////                 HSV hsv_white = {HSV_WHITE};
////                 if (hsv_white.v > rgb_matrix_get_val()) {
////                     hsv_white.v = rgb_matrix_get_val();
////                 }
////                 RGB rgb_white = hsv_to_rgb(hsv_white);
////                 RGB_MATRIX_INDICATOR_SET_COLOR(i, rgb_white.r, rgb_white.g, rgb_white.b);
////
////             } else {
////                 if (i > 30) {
////                     RGB_MATRIX_INDICATOR_SET_COLOR(i, rgb.r, rgb.g, rgb.b);
////                 } else {
////                     HSV hsv_red = {HSV_RED};
////                     if (hsv_red.v > rgb_matrix_get_val()) {
////                         hsv_red.v = rgb_matrix_get_val();
////                     }
////                     RGB rgb_red = hsv_to_rgb(hsv_red);
////                     RGB_MATRIX_INDICATOR_SET_COLOR(i, rgb_red.r, rgb_red.g, rgb_red.b);
////                 }
////             }
////             */
////         // if (HAS_ANY_FLAGS(g_led_config.flags[i], led_type)) {
////         //     uprintf("led %d has flags %d", i, led_type);
////         // }
////     }
////
////     // switch (mode) {
////     //     case 1: // breathing
////     //     {
////     //         uint16_t time = scale16by8(g_rgb_timer, speed / 8);
////     //         hsv.v         = scale8(abs8(sin8(time) - 128) * 2, hsv.v);
////     //         RGB rgb       = rgb_matrix_hsv_to_rgb();
////     //         for (uint8_t i = 0; i < RGB_MATRIX_LED_COUNT; i++) {
////     //             if (HAS_FLAGS(g_led_config.flags[i], led_type)) {
////     //                 RGB_MATRIX_INDICATOR_SET_COLOR(i, rgb.r, rgb.g, rgb.b);
////     //             }
////     //         }
////     //         break;
////     //     }
////     //     default: // Solid Color
////     //     {
////     //         RGB rgb = rgb_matrix_hsv_to_rgb();
////     //         for (uint8_t i = 0; i < RGB_MATRIX_LED_COUNT; i++) {
////     //             if (HAS_FLAGS(g_led_config.flags[i], led_type)) {
////     //                 RGB_MATRIX_INDICATOR_SET_COLOR(i, rgb.r, rgb.g, rgb.b);
////     //             }
////     //         }
////     //         break;
////     //     }
////     // }
//// }
////
//// void rgb_matrix_set_led_flags(void) {
////     for (uint8_t i = 0; i < 14; i++) {
////         g_led_config.flags[i] = LED_FLAG_UNDERGLOW;
////     }
//// }
////
//// bool rgb_matrix_indicators_advanced_user(uint8_t led_min, uint8_t led_max) {
////     // if (!rgb_matrix_indicators_keymap()) {
////     //     return false;
////     // }
////
//// #ifdef RGB_USE_IND_FUNC
////     if (userspace_config.rgb_layer_change) {
////         HSV hsv = get_layer_hsv(layer_state);
////
////         // uint8_t index = g_led_config.matrix_co[i][j];
////         // hsv.h         = hsv.h + (index * 10);
////         // RGB rgb = get_layer_rgb(layer_state);
////         // uint8_t mode = RGB_MATRIX_SOLID_COLOR;
////         // uint8_t flag = LED_FLAG_UNDERGLOW;
////         // rgb_matrix_set_by_flag(hsv, mode, flag, led_min, led_max);
////         // rgb_matrix_layer_helper(HSV_PURPLE, 1, rgb_matrix_config.speed, LED_FLAG_ALL, led_min, led_max);
////         rgb_matrix_layer_helper(hsv.h, hsv.s, hsv.v, 1, rgb_matrix_config.speed, LED_FLAG_UNDERGLOW, led_min, led_max);
////     }
//// #endif
////     return rgb_matrix_indicators_keymap();
//// }
////
#endif
////

/*
 * @header Copyright 2024 Taylor Premo (pdpt@gitlab.com) <pdpt@pm.me>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * @file        	pdpt.h
 */
#ifndef PDPT_H_
#define PDPT_H_
#include QMK_KEYBOARD_H

#include <stdint.h>
// #include "action_layer.h"
// #include "action.h"

// #include "process_keys/keymap_macros.h"
// #include "process_keys/process_records.h"
// #include "rgb/rgb_matrix_stuff.h"
// #include "callbacks.h"

/** define layer names */
enum layer_names {
    _BASE,
    _LOWER,
    _RAISE,
    _NUMPAD,
    _NAV,
    _ADJUST,
};

typedef union {
    uint32_t raw;
    struct {
        bool rgb_layer_change : 1;
        // bool     is_overwatch         : 1;
        bool nuke_switch : 1;
        bool rgb_matrix_idle_anim : 1;
        // bool     mouse_jiggler        : 1;
        bool matrix_scan_print : 1;
        bool align_reserved : 1;
        // uint8_t  oled_brightness      : 8;
        // bool     oled_lock            : 1;
        bool enable_acceleration : 1;
        // uint32_t reserved             : 13;
        bool    check : 1;
        uint8_t counter : 8;
    };
} userspace_config_t;

_Static_assert(sizeof(userspace_config_t) == sizeof(uint32_t), "Userspace EECONFIG out of spec.");

// not really sure how this works
extern userspace_config_t userspace_config;

void test(void);

#endif /* !PDPT_H_ */

# PDPT Userspace

personal userspace to keep code a bit more organized.

## structure

* `process_keys/`
  * `keymap_macros.h` - defines macros to make writing keymaps easier.
  * `process_records.c`  - `process_record_user` and similar functions.
* `rgb/` <!-- todo: -->
* `callbacks.c/h` - self explanatory, also contains init functions right now.
* `config.h` <!-- todo: -->
* `pdpt.c` - custom functions that don't belong anywhere else.
* `pdpt.h` - imports other userspace `.h` files,
  * also defines layer names.
  * and `userspace_config_t`
* `post_config.h` - self explanatory, seems to be needed because `config.h` doesn't keep definition order.
* `rgb_matrix_user.inc` - includes custom animations, I do not beleive this method works with the matrix indicator functions.
* `rules.mk` <!-- todo -->
* `template.c/h` - seems to be function definitions, but isn't actually live code.

### rules.mk

rules.mk - contains `SRC += $(USER_PATH)/pdpt.c`
pdpt.h - imports the other files in the userspace.
config.h - custom settings that I want regardless of keyboard.

## todo

* home row mods
  * G A S C / ◆ ⎇ ⇧ ⎈

```c


LGUI_T(KC_A), LALT_T(KC_S), LSFT_T(KC_D), LCTL_T(KC_F),

RCTL_T(KC_J), RSFT_T(KC_K), LALT_T(KC_L), RGUI_T(KC_SCLN)

```

* reduce the QUICK_TAP_TERM from its default value of TAPPING_TERM into a much smaller value, or even 0 to disable the auto-repeat feature entirely.
* PERMISSIVE_HOLD sounds ideal
* retro tap sounds good, but complicated to start with

* underglow highlight function
  * this needs to use matrix_indicators I believe
* specific key RGB control
* email on a layer
* scroll wheel
* enter/tab on both sides
* backspace + adjacent key for del
* <https://configure.zsa.io/moonlander/layouts/x6oxR/latest/3>
  * lots of nav options
  * has blocks that switch hands depending on layer
* <https://github.com/manna-harbour/miryoku/tree/master/docs/reference>
  * *probably* dont want anything from here, but worth looking at anyways
* should mirror layers so I can use either hand
  * stuff like F keys, nav, and numpad would be nice to have on either hand.
* oneshot to layer that leads to other layers?
* better delete options
* better bottom row layout
* mnemonic hotkeys (u for underscore)?
* better solution for semi-colon key
* gamer layer
* / as shift mod
* tri-layer in keymap.c

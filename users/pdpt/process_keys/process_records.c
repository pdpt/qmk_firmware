/*
 * @header Copyright 2024 Taylor Premo (pdpt@gitlab.com) <pdpt@pm.me>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * @file        	process_records.c
 */

#include "process_records.h"

#include "quantum/action.h"
#include "quantum/logging/print.h"
#include "quantum/rgb_matrix/rgb_matrix.h"

/**
 * @brief When you want to override the behavior of an existing key, or define
 * the behavior for a new key
 *
 * @details
 *      These are called by QMK during key processing before the actual key
 * event is handled. If these functions return true QMK will process the
 * kenycodes as usual. That can be handy for extending the functionality of a
 * key rather than replacing it. If these functions return false QMK will skip
 * the normal key handling, and it will be up to you to send any key up or down
 * events that are required.
 *

 * @param keycode
 * @param record
 * @return true             QMK will continue to process the keycodes as usual.
 * @return false            QMK will skip normal key handling.
 */
bool process_record_user(uint16_t keycode, keyrecord_t* record) {
// If console is enabled, it will print the matrix position and status of each
// key pressed
#ifdef CONSOLE_ENABLE
#    ifdef DEBUG_FUNCTIONS
    // print_function("process_record_user");
#    endif
    uint16_t mode = rgb_matrix_get_mode();
    // uprintf("KL: kc: 0x%04X, col: %2u, row: %2u, pressed: %u, time: %5u, int:
    // %u, count: %u\n", keycode, record->event.key.col, record->event.key.row,
    // record->event.pressed, record->event.time, record->tap.interrupted,
    // record->tap.count);
    uprintf("mode: %u\n", mode);
#endif

    return TRUE;
}

/**
 * @header Copyright 2024 Taylor Premo (pdpt@gitlab.com) <pdpt@pm.me>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * @file     keymap_macros.h
 *
 * macros to make keymaps a bit easier to write & more readable
 */

#pragma once
// #include "pdpt.h"
#include QMK_KEYBOARD_H
// clang-format off
#define LAYOUT_ortho_5x15_wrapper(...)       LAYOUT_ortho_5x15(__VA_ARGS__)
#define LAYOUT_ortho_5x15_base( \
         K00,     K01,    K02,      K03,     K04,     K05,     K06,     K07,     K08,      K09,    K0A,     K0B,     K0C,     K0D,     K0E, \
         K10,     K11,    K12,      K13,     K14,     K15,     K16,     K17,     K18,      K19,    K1A,     K1B,     K1C,     K1D,     K1E, \
         K20,     K21,    K22,      K23,     K24,     K25,     K26,     K27,     K28,      K29,    K2A,     K2B,     K2C,     K2D,     K2E, \
         K30,     K31,    K32,      K33,     K34,     K35,     K36,     K37,     K38,      K39,    K3A,     K3B,     K3C,     K3D,     K3E, \
         K40,     K41,    K42,      K43,     K44,     K45,     K46,     K47,     K48,      K49,    K4A,     K4B,     K4C,     K4D,     K4E \
  ) \
  LAYOUT_ortho_5x15_wrapper(                                                                                                                \
        KC_A,    KC_B,    KC_C,    KC_D,    KC_E,    KC_F,    KC_G,    KC_H,    KC_I,    KC_J,    KC_K,    KC_L,    KC_J,    KC_K,    KC_L, \
        KC_A,    KC_B,    KC_C,    KC_D,    KC_E,    KC_F,    KC_G,    KC_H,    KC_I,    KC_J,    KC_K,    KC_L,    KC_J,    KC_K,    KC_L, \
        KC_A,    KC_B,    KC_C,    KC_D,    KC_E,    KC_F,    KC_G,    KC_H,    KC_I,    KC_J,    KC_K,    KC_L,    KC_J,    KC_K,    KC_L, \
        KC_A,    KC_B,    KC_C,    KC_D,    KC_E,    KC_F,    KC_G,    KC_H,    KC_I,    KC_J,    KC_K,    KC_L,    KC_J,    KC_K,    KC_L, \
        KC_A,    KC_B,    KC_C,    KC_D,    KC_E,    KC_F,    KC_G,    KC_H,    KC_I,    KC_J,    KC_K,    KC_L,    KC_J,    KC_K,    KC_L  \
    )                                                                                                                                       \

#define LAYOUT_base_wrapper(...)       LAYOUT_ortho_5x15_base(__VA_ARGS__)

/**
 *
 * @brief blocks to group common keycodes together, to make keymaps easier to make and more readable.
 *
 * they are sized to match the 7 width keycodes.
 */

/* ========================================================================== */
/*                                home row mods                               */
/* ========================================================================== */

 // Left-hand home row mods
#define GUI_A LGUI_T(KC_A)
#define ALT_S LALT_T(KC_S)
#define SFT_D LSFT_T(KC_D)
#define CTL_F LCTL_T(KC_F)

// Right-hand home row mods
#define CTL_J RCTL_T(KC_J)
#define SFT_K RSFT_T(KC_K)
#define ALT_L LALT_T(KC_L)
#define GUI_SCLN RGUI_T(KC_SCLN)
#define GUI_QUOT RGUI_T(KC_QUOT)

/*                                mod left row                                */
#define _________home_row_mods_asdfg____________    GUI_A,   ALT_S,   SFT_D,   CTL_F,    KC_G
/*                                mod right row                               */
#define ______home_row_mods_hjkl_scln___________     KC_H,   CTL_J,   SFT_K,   ALT_L,GUI_SCLN


/* ------------------------------- left blocks ------------------------------ */
#define _________________12345__________________     KC_1,    KC_2,    KC_3,    KC_4,    KC_5
#define _________________qwert__________________     KC_Q,    KC_W,    KC_E,    KC_R,    KC_T
#define _________________asdfg__________________     KC_A,    KC_S,    KC_D,    KC_F,    KC_G
#define _________________zxcvb__________________     KC_Z,    KC_X,    KC_C,    KC_V,    KC_B

/* ------------------------------ right blocks ------------------------------ */
#define _________________67890__________________     KC_6,    KC_7,    KC_8,    KC_9,    KC_0
#define _________________yuiop__________________     KC_Y,    KC_U,    KC_I,    KC_O,    KC_P
#define _____________hjkl______________              KC_H,    KC_J,    KC_K,    KC_L
#define _____nm______                                KC_N,    KC_M
// clang-format on

/*
 * @header Copyright 2024 Taylor Premo (pdpt@gitlab.com) <pdpt@pm.me>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * @file        	process_records.h
 */

#ifndef PROCESS_RECORDS_H_
#define PROCESS_RECORDS_H_

#include "quantum/action.h"

bool process_record_user(uint16_t keycode, keyrecord_t* record);

#endif /* !PROCESS_RECORDS_H_ */

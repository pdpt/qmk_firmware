//===----------------------------------------------------------------------===//
/// \users\pdpt\keymap_macros.c
///
/// Copyright 2024 Taylor Premo tpre@pm.me
/// gitlab.com/pdpt
/// SPDX-License-Identifier: GPL-3.0-or-later
///
/// ===----------------------------------------------------------------------===
///
/// \file
/// <<fileoverview>>
///
//===----------------------------------------------------------------------===//

#include "bitwise.h"
#include QMK_KEYBOARD_H
#include "action_layer.h"
#include "rgb_functions.c"

#ifndef LAYER_NAMES_ENUM
#define LAYER_NAMES_ENUM
enum layer_names { _BASE, _LOWER, _RAISE, _NUMPAD, _NAV, _ADJUST, };
#endif


#if defined(ENCODER_ENABLE) && defined(ENCODER_MAP_ENABLE)
const uint16_t PROGMEM encoder_map[][NUM_ENCODERS][NUM_DIRECTIONS] = {

};
#endif // defined(ENCODER_ENABLE) && defined(ENCODER_MAP_ENABLE)



/**
 * @brief When you want to override the behavior of an existing key, or define the behavior for a new key
 *
 * @details
 *      These are called by QMK during key processing before the actual key event is handled.
 *      If these functions return true QMK will process the keycodes as usual.
 *      That can be handy for extending the functionality of a key rather than replacing it.
 *      If these functions return false QMK will skip the normal key handling,
 *      and it will be up to you to send any key up or down events that are required.
 *
 * @param keycode
 * @param record
 * @return true             QMK will continue to process the keycodes as usual.
 * @return false            QMK will skip normal key handling.
 */
bool process_record_user(uint16_t keycode, keyrecord_t* record) {
// If console is enabled, it will print the matrix position and status of each key pressed
#ifdef CONSOLE_ENABLE
#    ifdef DEBUG_FUNCTIONS
    //// print_function("process_record_user");
#    endif
    uint16_t mode = rgb_matrix_get_mode()''
    uprintf("mode: %u\n", mode);
#endif

    return TRUE;
}

/**
 * @brief This runs code every time that the layers get changed. This can be useful for layer indication, or custom layer handling.
 *
 * @param state
 * @return layer_state_t
 */
layer_state_t layer_state_set_user(layer_state_t state) {
    // state = update_tri_layer_state(state, _LOWER, _RAISE, _ADJUST);

    uprintf("state: %d\n", state);
    uprintf("biton: %d\n", biton32(state));
    uprintf("_ADJUST: %d\n", _ADJUST);
    // todo: ask for help about the layer switching going to the *previous* layer
    // todo: watch for the flickering stuff.
    // todo: have this only change the underglow?
#if defined(RGB_MATRIX_ENABLE) && defined(RGB_MATRIX_CUSTOM_USER)
// set_rgb_layer(state);
#endif
  return state;
}

bool rgb_matrix_indicators_user(void) {
#ifdef CONSOLE_ENABLE
#    ifdef DEBUG_FUNCTIONS

    // print_function("rgb_matrix_indicators_user");
#    endif
#endif
    return true;
}

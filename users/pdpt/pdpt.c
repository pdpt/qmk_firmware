/*
 * @header Copyright 2024 Taylor Premo (pdpt@gitlab.com) <pdpt@pm.me>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * @file        	pdpt.c
 */

#include "pdpt.h"

#include "process_keys/keymap_macros.h"
#include "rgb/rgb_matrix_stuff.h"

#include "quantum/logging/print.h"
#include "quantum/rgb_matrix/rgb_matrix.h"

userspace_config_t userspace_config;

void test(void) {
    uprintf("// keymap_macros.h\n");
    uprintf("initial mode: %u%u%u%u%u\n", _________________12345__________________);

    uprintf("// process_records.h\n");
    process_record_user(0, 0);

    uprintf("// rgb_matrix_stuff.h\n");
    // in drashna's repo, this isn't used anywhere other than being overwritten.
    rgb_matrix_indicators_user();

    uprintf("// callbacks.h\n");
    // this would cause infinite loop
    // keyboard_post_init_user();
    shutdown_user(false);
    suspend_power_down_user();
    suspend_wakeup_init_user();
    layer_state_set_user(0);
    default_layer_state_set_user(0);
    led_set_user(0);
    housekeeping_task_user();

    uprintf("// done\n");

    /*
    typedef struct PACKED {
        uint8_t     matrix_co[MATRIX_ROWS][MATRIX_COLS];
        led_point_t point[RGB_MATRIX_LED_COUNT];
        uint8_t     flags[RGB_MATRIX_LED_COUNT];
    } led_config_t;
    */
    uprintf("g_led_config:\n");
    uprintf("RGB_MATRIX_LED_COUNT: %d\n", RGB_MATRIX_LED_COUNT);

    for (uint8_t index_var = 0; index_var < RGB_MATRIX_LED_COUNT; index_var++) {
        dprintf("i %d: f %d: p: (%d, %d), \n", index_var, g_led_config.flags[index_var], g_led_config.point[index_var].x, g_led_config.point[index_var].y);
        wait_ms(10);
    }
    // wait_ms(10000); // wait ten seconds so I can read.
}
